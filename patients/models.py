from django.db import models

class Patient(models.Model):
    name = models.CharField(max_length=128)
    age = models.PositiveIntegerField()
    national_id = models.CharField(max_length=10, unique=True)
    address = models.CharField(max_length=512)
    phone_number = models.CharField(max_length=15)

    def __str__(self) -> str:
        return self.name
