from rest_framework import permissions

class ClinicAccessPermission(permissions.BasePermission):
    """
    Global permission check for accessible clinics.
    """
    pass
