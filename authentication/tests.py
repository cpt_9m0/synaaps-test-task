from django.test import TestCase

from authentication.models import User
from clinics.models import Admission, Clinic, PolyClinic, SharedPatient
from patients.models import Patient


class ProfileTestCase(TestCase):
    def setUp(self) -> None:
        self.user = User.objects.create(username="secretary")
        self.polyclinic = PolyClinic.objects.create(name="Test PolyClinic")
        self.clinic = Clinic.objects.create(
            name="Test Clinic", polyclinic=self.polyclinic)
        self.clinic2 = Clinic.objects.create(
            name="Test Clinic", polyclinic=self.polyclinic)
        self.test_patient = Patient.objects.create(
            name="Ali",
            age=22,
            national_id="0770691235",
            address="Fake Address",
            phone_number="091649865887"
        )
        self.test_admission = Admission.objects.create(
            patient=self.test_patient,
            clinic=self.clinic,
            insurance_type="Test",
            insurance_number="6548646",
            complaint=" Headache"
        )
        self.test_patient2 = Patient.objects.create(
            name="Ali",
            age=22,
            national_id="0550691235",
            address="Fake Address",
            phone_number="091649865887"
        )
        self.test_admission2 = Admission.objects.create(
            patient=self.test_patient2,
            clinic=self.clinic2,
            insurance_type="Test",
            insurance_number="6548646",
            complaint=" Headache"
        )
        return super().setUp()

    def test_create_profile_signal(self):
        """
        Tests if create_profile signal works for User.
        """
        profile = self.user.profile
        self.assertIsNotNone(profile)
        self.assertEqual(profile.role, 0)

    def test_has_access_to_clinic(self):
        """
        Tests has_access_to_clinic function for profile instances.
        """
        profile = self.user.profile
        # Add clinic to user clinics.
        profile.clinics.add(self.clinic)
        self.assertTrue(profile.has_access_to_clinic(self.clinic))
        self.assertFalse(profile.has_access_to_clinic(self.clinic2))

    def test_has_access_to_patient(self):
        """
        Tests has_access_to_patient function for profile instances.
        """
        profile = self.user.profile
        # Add clinic to user clinics.
        profile.clinics.add(self.clinic)
        
        self.assertTrue(profile.has_access_to_patient(self.test_patient))
        self.assertFalse(profile.has_access_to_patient(self.test_patient2))

        # shared patient
        SharedPatient.objects.create(patient=self.test_patient2, clinic=self.clinic)
        with self.assertNumQueries(2):
            self.assertTrue(profile.has_access_to_patient(self.test_patient2))
