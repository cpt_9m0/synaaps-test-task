from django.db import models
from django.contrib.auth import get_user_model

from clinics.models import Admission, Clinic, SharedPatient
from patients.models import Patient

User = get_user_model()

class Profile(models.Model):
    class Roles(models.IntegerChoices):
        SECRETARY = 0, "Secretary"
        DOCTOR = 1, "Doctor"
    user = models.OneToOneField(to=User, on_delete=models.CASCADE)
    role = models.IntegerField(choices=Roles.choices, default=Roles.SECRETARY)
    clinics = models.ManyToManyField(to=Clinic)

    @property
    def readable_role(self):
        return self.Roles.choices[self.role][1]
    
    def has_access_to_clinic(self, clinic: Clinic):
        return self.clinics.filter(pk=clinic.pk).exists()
    
    def has_access_to_patient(self, patient: Patient):
        patient_clinics = Admission.objects.filter(patient=patient).values("clinic__pk")
        patients_shared = SharedPatient.objects.filter(patient=patient).values("clinic__pk")
        patient_in_clinic = self.clinics.filter(pk__in=patient_clinics).exists()
        is_shared = self.clinics.filter(pk__in=patients_shared).exists()
        return patient_in_clinic or is_shared

    
    def __str__(self) -> str:
        return f"{self.user}: {self.readable_role}"
