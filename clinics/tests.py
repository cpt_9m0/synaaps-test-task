from django.test import TestCase
from clinics.models import Admission, AdmissionStatus, Clinic, PolyClinic

from patients.models import Patient

class AdmissionTestCase(TestCase):
    def setUp(self) -> None:
        self.polyclinic = PolyClinic.objects.create(name="Test PolyClinic")
        self.clinic = Clinic.objects.create(
            name="Test Clinic", polyclinic=self.polyclinic)
        self.patient = Patient.objects.create(
            name="Ali",
            age=22,
            national_id="0770691235",
            address="Fake Address",
            phone_number="091649865887"
        )
        self.admission = Admission.objects.create(
            patient=self.patient,
            clinic=self.clinic,
            insurance_type="Test",
            insurance_number="6548646",
            complaint=" Headache"
        )
        return super().setUp()
    
    def test_create_status_signal(self):
        """
        Tests if create_status signal works for Admission.
        """
        status = AdmissionStatus.objects.all()
        self.assertTrue(status.exists())
        self.assertEqual(status.count(), 1)
        status = status.first()
        self.assertEqual(status.status, AdmissionStatus.Status.WAITING)
        self.assertEqual(status.admission, self.admission)
